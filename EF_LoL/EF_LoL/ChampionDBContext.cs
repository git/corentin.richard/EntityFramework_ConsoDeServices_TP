﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_LoL
{
    public class ChampionDBContext : DbContext
    {
        public ChampionDBContext()
        { }

        public ChampionDBContext(DbContextOptions<ChampionDBContext> options)
            : base(options)
        { }

        //ChampionEntity champ1 = new ChampionEntity("Bob");
        //ChampionEntity champ2 = new ChampionEntity("Fanta");

        public DbSet<ChampionEntity> ChampionEntity { get; set; }
        
        //protected override void OnConfiguring(DbContextOptionsBuilder options)
        //    => options.UseSqlite("Data Source= EF_LoL.MyDataBase.db");

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlite("Data Source= EF_LoL.MyDataBase.db");
            }
        }
    }
}
