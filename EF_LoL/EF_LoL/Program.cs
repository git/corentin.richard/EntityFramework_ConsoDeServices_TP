﻿// See https://aka.ms/new-console-template for more information
using EF_LoL;

Console.WriteLine("Hello, World!");


ChampionEntity chewie = new ChampionEntity("Chewbacca");
ChampionEntity yoda = new ChampionEntity ("Yoda");
ChampionEntity ewok = new ChampionEntity ("Ewok");

using (var context = new ChampionDBContext())
{
    // Crée des Champion et les insère dans la base
    Console.WriteLine("Creates and inserts new Champion");
    context.ChampionEntity.Add(chewie);
    context.ChampionEntity.Add(yoda);
    context.ChampionEntity.Add(ewok);
    context.SaveChanges();
}

using (var context = new ChampionDBContext())
{
    foreach (var n in context.ChampionEntity)
    {
        Console.WriteLine($"{n.Name} - {n.Bio} [ {n.Icon} ]");
    }
    context.SaveChanges();
}
