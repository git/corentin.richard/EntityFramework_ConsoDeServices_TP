﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace EF_LoL
{
    public class ChampionEntity
    {
        //dotnet ef database update --context ChampionDBContext --project EF_LoL
        [Key]
        public string Name
        {
            get => name;
            private init
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    name = "Unknown";
                    return;
                }
                name = value;
            }
        }
        private readonly string name = null!;

        public string Bio
        {
            get => bio;
            set
            {
                if (value == null)
                {
                    bio = "";
                    return;
                }
                bio = value;
            }
        }
        private string bio = "";

        public string Icon { get; set; }




        public ChampionEntity(string name, string icon = "", string bio = "")
        {
            Name = name;
            Icon = icon;
            Bio = bio;
            ///Characteristics = new ReadOnlyDictionary<string, int>(characteristics);
        }

        ///public ReadOnlyDictionary<string, int> Characteristics { get; private set; }
        ///private readonly Dictionary<string, int> characteristics = new Dictionary<string, int>();


        //  creer table avec db context et db set de champion
        //  puis ajout d'un nouveau champion
        //  pour ajouté un attribut : supprimer nos migrations et nos db

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (GetType() != obj.GetType()) return false;
            return Equals(obj as ChampionEntity);
        }

        public override int GetHashCode()
            => Name.GetHashCode() % 997;

        public bool Equals(ChampionEntity? other)
            => Name.Equals(other?.Name);

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder($"{Name}");
            if (!string.IsNullOrWhiteSpace(bio))
            {
                sb.AppendLine($"\t{bio}");
            }
            return sb.ToString();
        }



        ///// Characteristique

        ////public void AddCharacteristics(params Tuple<string, int>[] someCharacteristics)
        ////{
        ////    foreach (var c in someCharacteristics)
        ////    {
        ////        characteristics[c.Item1] = c.Item2;
        ////    }
        ////}

        ////public bool RemoveCharacteristics(string label)
        ////    => characteristics.Remove(label);

        ////public int? this[string label]
        ////{
        ////    get
        ////    {
        ////        if (!characteristics.TryGetValue(label, out int value)) return null;
        ////        else return value;
        ////    }
        ////    set
        ////    {
        ////        if (!value.HasValue)
        ////        {
        ////            RemoveCharacteristics(label);
        ////            return;
        ////        }
        ////        characteristics[label] = value.Value;
        ////    }
        ////}

    }
}
