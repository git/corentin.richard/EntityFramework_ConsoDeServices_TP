using EF_LoL;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;


namespace TestProject
{

    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var options = new DbContextOptionsBuilder<ChampionDBContext>()
                .UseInMemoryDatabase(databaseName: "Add_Test_database")
                .Options;

            //prepares the database with one instance of the context
            using (var context = new ChampionDBContext(options))
            {

                ChampionEntity chewie = new ChampionEntity("Chewbacca");
                ChampionEntity yoda = new ChampionEntity("Yoda");
                ChampionEntity ewok = new ChampionEntity("Ewok");


                Console.WriteLine("Creates and inserts new Champion for tests");
                context.ChampionEntity.Add(chewie);
                context.ChampionEntity.Add(yoda);
                context.ChampionEntity.Add(ewok);
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            using (var context = new ChampionDBContext(options))
            {
                Assert.Equal(3, context.ChampionEntity.Count());
                Assert.Equal("Chewbacca", context.ChampionEntity.First().Name);
            }
        }
       

    }
}