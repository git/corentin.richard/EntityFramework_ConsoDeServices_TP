using API_LoL;
using EntityFramework;
using EntityFramework.Manager;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Logging;
using Model;
using StubLib;

var builder = WebApplication.CreateBuilder(args);


//Versioning

///NOT WORKING WHEN CHANGING VERSIONS :
/// voir sur https://blog.christian-schou.dk/how-to-use-api-versioning-in-net-core-web-api/ rubrique "Configure SwaggerOptions"
/// (mais requiere l'injection de d�pendance).
/// Sinon, code plus simple disponible par le prof



// Add ApiExplorer to discover versions
builder.Services.AddVersionedApiExplorer(setup =>
{
    setup.GroupNameFormat = "'v'VVV";
    setup.SubstituteApiVersionInUrl = true;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.ConfigureOptions<ConfigureSwaggerOptions>();


builder.Services.AddApiVersioning(o => o.ApiVersionReader = new UrlSegmentApiVersionReader());

// Add services to the container.
builder.Services.AddControllers();



//builder.Services.AddScoped<IDataManager,StubData>();


builder.Services.AddHttpClient();

//builder.Services.AddScoped<IDataManager,StubData>();
builder.Services.AddScoped<IDataManager,EFDataManager>();

builder.Services.AddDbContext<LoLDBContextWithStub>();

var app = builder.Build();

using(var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetService<LoLDBContextWithStub>();
    context.Database.EnsureCreated();
}

var apiVersionDescriptionProvider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        
        foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions)
        {
            options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                description.GroupName.ToUpperInvariant());
        }
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
