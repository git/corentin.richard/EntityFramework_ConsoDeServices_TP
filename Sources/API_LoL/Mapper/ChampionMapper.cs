﻿using API_LoL.Mapper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Mapper
{
    public static class ChampionMapper
    {
        public static ChampionDTO ToDTO(this Champion champion)
        {
            return new ChampionDTO(champion.Name, champion.Bio, champion.Icon, champion.Class.ToDTO(), champion.Image.Base64);
        }

        public static Champion ToChampion(this ChampionDTO champion)
        {
            return new Champion(champion.Name, champClass: champion.Class.ToChampionClass(),icon: champion.Icon,bio: champion.Bio,image :champion.Image);

        }
    }
}
