﻿using DTO;
using Model;

namespace API_LoL.Mapper
{
    public static class RuneMapper
    {
        public static RuneDTO ToDTO(this Rune rune)
        {
            return new RuneDTO(rune.Name, rune.Description, rune.Family, rune.Image, rune.Icon);
        }

        public static Rune ToRune(this RuneDTO rune)
        {
            return new Rune(rune.Name, rune.Family, rune.Icon, rune.Image.Base64, rune.Description);

        }
    }
}
