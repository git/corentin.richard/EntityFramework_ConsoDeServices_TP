﻿using DTO;
using Model;

namespace API_LoL.Mapper
{
    public static class SkinMapper
    {
        public static SkinDTO ToDTO(this Skin skin)
        {
            return new SkinDTO(skin.Name, skin.Description, skin.Icon,skin.Image.Base64,skin.Price);
        }

        public static Skin ToSkin(this SkinDTO skin)
        {
            return new Skin(skin.Name, null,price: skin.Price, icon:skin.Icon,image: skin.Image,description: skin.Description) ;
        }
    }
}
