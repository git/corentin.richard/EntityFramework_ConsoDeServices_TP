﻿using DTO;
using Model;

namespace API_LoL.Mapper
{
    public static class ChampionClassMapper
    {
        public static string ToDTO(this ChampionClass championClass)
        {
            return championClass.ToString();
        }

        public static ChampionClass ToChampionClass(this String championClass)
        {
            switch (championClass)
            {
                case "Assassin":
                    return ChampionClass.Assassin;
                case "Fighter":
                    return ChampionClass.Fighter;
                case "Mage":
                    return ChampionClass.Mage;
                case "Marksman":
                    return ChampionClass.Marksman;
                case "Support":
                    return ChampionClass.Support;
                case "Tank":
                    return ChampionClass.Tank;
                default:
                    return ChampionClass.Unknown;
            }
        }
    }
}
