﻿using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using DTO;
using DTO.Mapper;
using System.CodeDom.Compiler;

namespace API_LoL.Controllers
{
    [ApiVersion("2.0")]
    [ApiVersion("2.2")]
    [Route("api/v{version:apiVersion}/versioned")]
    [ApiController]
    public class ChampionsControllerVersioned : ControllerBase
    {
        public ChampionsControllerVersioned(IDataManager Manager)
        {
            this.ChampionsManager = Manager.ChampionsMgr;
        }

        private IChampionsManager ChampionsManager;

        // GET api/<ChampionController>/5

        //[HttpGet]
        //public async Task<IActionResult> Get(String? name = null, String? skill = null, String? characteristic = null, int index = 0, int size = 10)
        //{
        //    if (size - index > 10)
        //    {
        //        return BadRequest();
        //    }
        //    if (!string.IsNullOrEmpty(name))
        //    {
        //        var list = await ChampionsManager.GetItemsByName(name, index, size);
        //        if (list.Count() != 0)
        //        {
        //            return Ok(list.Select(champion => champion?.ToDTO()));
        //        }
        //        else { return NoContent(); }
        //    }
        //    else if (!string.IsNullOrEmpty(skill))
        //    {
        //        var list = await ChampionsManager.GetItemsBySkill(skill, index, size);
        //        if (list.Count() != 0)
        //        {
        //            return Ok(list.Select(champion => champion?.ToDTO()));
        //        }
        //        else { return NoContent(); }
        //    }
        //    else if (!string.IsNullOrEmpty(characteristic))
        //    {
        //        var list = await ChampionsManager.GetItems(index, size);
        //        if (list.Count() != 0)
        //        {
        //            return Ok(list.Select(champion => champion?.ToDTO()));
        //        }
        //        else { return NoContent(); }
        //    }
        //    else
        //    {
        //        var list = await ChampionsManager.GetItems(index, size);
        //        if (list.Count() != 0)
        //        {
        //            return Ok(list.Select(champion => champion?.ToDTO()));
        //        }
        //        else { return NoContent(); }
        //    }
        //}

        
        //// POST api/<ChampionController>
        //[HttpPost]
        //public async Task<IActionResult> Post(ChampionDTO champion)
        //{
        //    if (champion == null)
        //    {
        //        return UnprocessableEntity();
        //    }
        //    else
        //    {
        //        await ChampionsManager.AddItem(champion.ToChampion());
        //        return CreatedAtAction("Post", champion);
        //    }
        //}

        //// PUT api/<ChampionController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<ChampionController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}


        ///---------- Versioning ----------///
        [HttpGet, MapToApiVersion("2.0")]
        public string GetThatOnlySayHello() => "Hello v2.0!";


        //! FIXME : not working, mais avec la version 2.0 ca marche ! 
        [HttpGet, MapToApiVersion("2.2")]
        public string GetThatOnlySayHelloV2() => "Hello but i'm from v2.2!";
    }
}
