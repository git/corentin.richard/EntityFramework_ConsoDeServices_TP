﻿using API_LoL.Mapper;
using DTO;
using EntityFramework;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace API_LoL.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class RunesController : ControllerBase
    {
       

        public RunesController(IDataManager Manager)
        {
            this.RunesManager = Manager.RunesMgr;
        }

        private IRunesManager RunesManager;

        // GET api/<ChampionController>/5

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            return Ok(RunesManager.GetNbItems());
        }

        [HttpGet]
        public async Task<IActionResult> Get(string? name = null, string desc = null, EnumRuneFamily Family = EnumRuneFamily.Unknown, LargeImage Image = null, int index = 0, int size = 10)
        {
            if (size - index > 10)
            {
                return BadRequest();
            }
            if (!string.IsNullOrEmpty(name))
            {
                var list = await RunesManager.GetItemsByName(name, index, size);
                if (list.Count() != 0)
                {
                    return Ok(list.Select(rune => rune?.ToDTO()));
                }
                else { return NoContent(); }
            }
            else
            {
                var list = await RunesManager.GetItems(index, size);
                if (list.Count() != 0)
                {
                    return Ok(list.Select(champion => champion?.ToDTO()));
                }
                else { return NoContent(); }
            }
        }

        [HttpGet("name")]
        public async Task<IActionResult> GetByName(String name)
        {
            if (string.IsNullOrEmpty(name)) return BadRequest();
            var list = await RunesManager.GetItemsByName(name, 0, 1);
            if (list.Count() == 1)
            {
                return Ok(list.Select(rune => rune?.ToDTO()).First());
            }
            else { return NoContent(); }

        }



        // POST api/<ChampionController>
        [HttpPost]
        public async Task<IActionResult> Post(RuneDTO rune)
        {
            if (rune == null)
            {
                return UnprocessableEntity();
            }
            else
            {
                var champ = await RunesManager.GetItemsByName(rune.Name, 0, 1);
                if(champ.Count() > 0)
                    if (champ.Where(c => c.Name == rune.Name).Count() == 1)
                    {
                        return Conflict(rune);
                    }
                await RunesManager.AddItem(rune.ToRune());
                return CreatedAtAction("Post", rune);

            }
        }

        // PUT api/<ChampionController>/5
        [HttpPut("name")]
        public async Task<IActionResult> Put(string name, RuneDTO rune)
        {
            if (string.IsNullOrEmpty(name))
                return BadRequest();
            if (rune == null)
                return UnprocessableEntity();
            var list = await RunesManager.GetItemsByName(name, 0, 1);
            if (list.Count() == 1)
            {
                return Ok(RunesManager.UpdateItem(list.First(), rune.ToRune()));
            }
            else { return NoContent(); }
        }

        // DELETE api/<ChampionController>/5
        [HttpDelete("name")]
        public async Task<IActionResult> Delete(string name)
        {
            if (string.IsNullOrEmpty(name))
                return BadRequest();
            var list = await RunesManager.GetItemsByName(name, 0, 1);
            if (list.Count() == 1)
            {
                return Ok(await RunesManager.DeleteItem(list.First()));
            }
            else { return NoContent(); }
        }
    }
}
