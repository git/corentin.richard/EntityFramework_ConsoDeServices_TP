﻿// See https://aka.ms/new-console-template for more information
using System.Net.Security;
using Model;
using System.Net.Http;
using System.Reflection.Metadata;
using Newtonsoft.Json;
using DTO;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Json;

class APIResponse
{
    public string status { get; set; }
    public List<string> champions { get; set; }
}


static class Program
{

    static HttpClient client = new HttpClient();
    static async Task Main(string[] args) {
       HttpClient client = new HttpClient();
        await DisplayMainMenu();
    }

    public static async Task DisplayMainMenu()
    {
        Dictionary<int, string> choices = new Dictionary<int, string>()
        {
            [1] = "1- Manage Champions",
            [2] = "2- Manage Skins",
            [3] = "3- Manage Runes",
            [4] = "4- Manage Rune Pages",
            [99] = "99- Quit"
        };

        while (true)
        {
            int input = DisplayAMenu(choices);

            switch (input)
            {
                case 1:
                    await DisplayChampionsMenu();
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 99:
                    Console.WriteLine("Bye bye!");
                    return;
                default:
                    break;
            }
        }
    }

    private static int DisplayAMenu(Dictionary<int, string> choices)
    {
        int input = -1;
        while (true)
        {
            Console.WriteLine("What is your choice?");
            Console.WriteLine("--------------------");
            foreach (var choice in choices.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Value))
            {
                Console.WriteLine(choice);
            }
            if (!int.TryParse(Console.ReadLine(), out input) || input == -1)
            {
                Console.WriteLine("I do not understand what your choice is. Please try again.");
                continue;
            }
            break;
        }
        Console.WriteLine($"You have chosen: {choices[input]}");
        Console.WriteLine();
        return input;
    }

    public static async Task DisplayChampionsMenu()
    {
        Dictionary<int, string> choices = new Dictionary<int, string>()
        {
            [0] = "0- Get number of champions",
            [1] = "1- Get champions",
            [2] = "2- Find champions by name",
            [3] = "3- Find champions by characteristic",
            [4] = "4- Find champions by class",
            [5] = "5- Find champions by skill",
            [6] = "6- Add new champion",
            [7] = "7- Delete a champion",
            [8] = "8- Update a champion",
        };

        int input = DisplayAMenu(choices);

        switch (input)
        {
            case 0:
                
            case 1:
                {
                    var response = await client.GetFromJsonAsync<Object>(
                        "https://localhost:7144/api/Champions");

                    

                    Console.WriteLine(response.ToString());
                    string? json = response.ToString();
                    List<ChampionDTO> f = System.Text.Json.JsonSerializer.Deserialize<List<ChampionDTO>>(json: json);
                    foreach(var c in f)
                    {
                        Console.WriteLine(c.ToString());
                    }
                
                }
                break;
            case 2:
                {
                    
                }
                break;
            case 3:
                {
                    
                }
                break;
            case 4:
                {
                    
                }
                break;
            case 5:
                {
                    
                }
                break;
            case 6:
                {
                    
                }
                break;
            case 7:
                {
                    
                }
                break;
            case 8:
                {
                    
                }
                break;
            default:
                break;
        }

    }

    public static void DisplayCreationChampionMenu(Champion champion)
    {
        Dictionary<int, string> choices = new Dictionary<int, string>()
        {
            [1] = "1- Add a skill",
            [2] = "2- Add a skin",
            [3] = "3- Add a characteristic",
            [99] = "99- Finish"
        };

        while (true)
        {
            int input = DisplayAMenu(choices);

            switch (input)
            {
                case 1:

                case 2:

                case 3:

                case 99:
                    return;
                default:
                    break;
            }
        }
    }
}