﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SkinDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }

        public string Image { get; set; }

        public float Price { get; set; }

        public SkinDTO(string name,string description,string icon,string image,float price) { 
            this.Name = name;
            this.Description = description;
            this.Icon = icon;
            this.Image = image;
            this.Price = price;
        
        }
    }
}
