﻿using Model;
using System.Collections.Immutable;

namespace DTO
{
    public class ChampionDTO
    {
        public ChampionDTO(string name, string bio, string icon, string Class, string image)
        {
            Name = name;
            Bio = bio;
            Icon = icon;
            this.Class = Class;
            Image = image;
        }
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Icon { get; set; }

        public string Image { get; set; }
        public string Class { get; set; }

        public bool equals(ChampionDTO other)
        {
            return other.Name==this.Name && other.Bio==this.Bio && other.Icon==this.Icon;
        }

        public string toString()
        {
            return Name + Bio + Icon;
        }
    }
}