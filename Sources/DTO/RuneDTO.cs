﻿using EntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class RuneDTO
    {
        public RuneDTO(string name, string desc, RuneFamily family, LargeImage image, string icon)
        {
            Name = name;
            Description = desc;
            Family = family;
            Image = image;
            Icon = icon;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public RuneFamily Family { get; set; }
        public string Icon { get; set; }

        public LargeImage Image { get; set; }        

        public bool equals(RuneDTO other)
        {
            return other.Name == Name
                && other.Description == Description
                && other.Family == Family
                && other.Image == Image
                && other.Icon == Icon;
        }

        public string toString()
        {
            return Name + Description + Family.ToString();
        }
    }
}
