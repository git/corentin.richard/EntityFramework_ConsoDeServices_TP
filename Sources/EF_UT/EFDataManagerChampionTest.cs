﻿using EntityFramework;
using EntityFramework.Manager;
using FluentAssertions;
using FluentAssertions.Primitives;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_UT
{
    [TestClass]
    public class EFDataManagerChampionTest
    {
        [TestMethod]
        public void Add_ValidChampion_Added()
        {
            IDataManager dataManager = new EFDataManager();
            IChampionsManager championsManager = dataManager.ChampionsMgr;

            var champ = championsManager.AddItem(new Champion("test"));
        }

        [TestMethod]
        public async Task GetItemsByName_DefaultChamp_One()
        {
            var builder = WebApplication.CreateBuilder();

            builder.Services.AddDbContext<LoLDBContextWithStub>();
            
            var app = builder.Build();

            using (var scope = app.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<LoLDBContextWithStub>();
                context.Database.EnsureCreated();
            }

            IDataManager dataManager = new EFDataManager();
            IChampionsManager championsManager = dataManager.ChampionsMgr;

            var ak = (await championsManager.GetItemsByName("A", 0, 1)).First();

            Assert.IsNotNull(ak);
            Assert.AreEqual("Aatrox", ak.Name);
        }
    }
}
