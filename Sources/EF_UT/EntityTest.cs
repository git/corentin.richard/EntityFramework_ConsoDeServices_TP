﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntityFramework;
using System.Threading.Tasks;


namespace EF_UT
{
    [TestClass]
    public class EntityTest
    {
        [TestMethod]
        public void TestAdd()
        {
            var options = new DbContextOptionsBuilder<LoLDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_Test_database").Options;


            //prepares the database with one instance of the context
            using (var context = new LoLDbContext(options))
            {

                ChampionEntity chewie = new ChampionEntity { Name = "Chewbacca", Bio = "", Icon = "" };
                ChampionEntity yoda = new ChampionEntity{ Name = "Yoda", Bio = "", Icon = "" };
                ChampionEntity ewok = new ChampionEntity{ Name = "Ewok", Bio = "", Icon = "" };

                //SkinEntity defaulSkin = new SkinEntity("Skin Default", chewie);
                //chewie.AddSkin(defaulSkin);
                Console.WriteLine("Creates and inserts new Champion for tests");
                context.Add(chewie);
                context.Add(yoda);
                context.Add(ewok);
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            using (var context = new LoLDbContext(options))
            {
                Assert.AreEqual(3, context.Champions.Count());
                Assert.AreEqual("Chewbacca", context.Champions.First().Name);
            }
        }


        [TestMethod]
        public void TestUpdate()
        {
            var options = new DbContextOptionsBuilder<LoLDbContext>()
                .UseInMemoryDatabase(databaseName: "Modify_Test_database")
                .Options;

            //prepares the database with one instance of the context
            using (var context = new LoLDbContext(options))
            {
                ChampionEntity chewie = new ChampionEntity{ Name = "Chewbacca", Bio = "ewa", Icon = "" };
                ChampionEntity yoda = new ChampionEntity{ Name = "Yoda", Bio = "wewo", Icon = "" };
                ChampionEntity ewok = new ChampionEntity{ Name = "Ewok", Bio = "", Icon = "" };   

                context.Add(chewie);
                context.Add(yoda);
                context.Add(ewok);
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            using (var context = new LoLDbContext(options))
            {
                string BioToFind = "ew";
                Assert.AreEqual(2, context.Champions.Where(n => n.Bio.ToLower().Contains(BioToFind)).Count());
                BioToFind = "ewo";
                Assert.AreEqual(1, context.Champions.Where(n => n.Bio.ToLower().Contains(BioToFind)).Count());
                var ewok = context.Champions.Where(n => n.Bio.ToLower().Contains(BioToFind)).First();
                ewok.Bio = "Wicket";
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            //using (var context = new LoLDbContext(options))
            //{
            //    string NameToFind = "ew";
            //    Assert.AreEqual(1, context.Champions.Where(n => n.Bio.ToLower().Contains(NameToFind)).Count());
            //    NameToFind = "wick";
            //    Assert.AreEqual(1, context.Champions.Where(n => n.Bio.ToLower().Contains(NameToFind)).Count());
            //}
        }
    }
}
