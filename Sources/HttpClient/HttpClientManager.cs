﻿using Model;

namespace HttpClient
{
    public partial class HttpClientManager : IDataManager
    {


        public System.Net.Http.HttpClient httpC { get; set; } = new System.Net.Http.HttpClient();


        public HttpClientManager() { 
            ChampionsMgr = new ChampionManager(this, httpC);
            httpC.BaseAddress = new Uri("https://localhost:7144/api/");

        }

        public ISkinsManager SkinsMgr => throw new NotImplementedException();

        public IRunesManager RunesMgr => throw new NotImplementedException();

        public IRunePagesManager RunePagesMgr => throw new NotImplementedException();

        public IChampionsManager ChampionsMgr { get; set; }

    }
}
