﻿using DTO;
using DTO.Mapper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Reflection;
using System.Reflection.Metadata;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HttpClient
{
    public partial class HttpClientManager
    {
        public class ChampionManager : IChampionsManager
        {


            private readonly HttpClientManager parent;
            
            private System.Net.Http.HttpClient httpc;

            public string BaseAddress;


            public ChampionManager(HttpClientManager parent, System.Net.Http.HttpClient httpc) {

                this.httpc = httpc;
                this.parent = parent;
            }

            public async Task<Champion?> AddItem(Champion? item) //return le champion ajouté, null sinon ?
            {
                if(item==null) throw new ArgumentNullException("item is null");
                var response = await httpc.PostAsJsonAsync("v1/Champions?Name = " + item.Name, item);

                return response.IsSuccessStatusCode ? item : null;
            }

            public async Task<bool> DeleteItem(Champion? item)
            {
                HttpResponseMessage response = await httpc.DeleteAsync("v1/Champions?Name=" + item.Name);
                _ = response.StatusCode;
                return response.IsSuccessStatusCode;
            }

            public async Task<IEnumerable<Champion?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {   
                
                IEnumerable<ChampionDTO> champdto = await httpc.GetFromJsonAsync<IEnumerable<ChampionDTO>>("v1/Champions?index="+index+"&size="+count);
                return champdto.Select(c => c.ToChampion());
            }





            public Task<IEnumerable<Champion?>> GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                return httpc.GetFromJsonAsync<IEnumerable<Champion>>("v1/Champions?name=" + substring+"&index=" + index + "&size=" + count);
            }

            public async Task<int> GetNbItems()
            {
                //HttpResponseMessage response = 
                //Console.WriteLine(response.Content.ToString());
                //return int.Parse(response.Content.ToString());
                //return await httpc.GetFromJsonAsync<int>("v1/Champions/count");
                var response = await httpc.GetFromJsonAsync<ChampionCountResponse>("v1/Champions/count");
                return response.Result;
            }


            public Task<IEnumerable<Champion?>> GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }
        

            public Task<int> GetNbItemsByCharacteristic(string charName)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByClass(ChampionClass championClass)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByName(string substring)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByRunePage(RunePage? runePage)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsBySkill(Skill? skill)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsBySkill(string skill)
            {
                throw new NotImplementedException();
            }

            public async Task<Champion?> UpdateItem(Champion? oldItem, Champion? newItem)
            {
                HttpResponseMessage rep = await httpc.PutAsJsonAsync("v1/Champions?name=" + oldItem.Name, newItem.ToDTO());

                if (rep.IsSuccessStatusCode) return newItem;
                //else
                return null;
            }
        }
    }
}
