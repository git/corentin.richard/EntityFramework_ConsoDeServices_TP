using API_LoL.Controllers;
using DTO;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;
using Model;
using StubLib;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Api_UT
{
    [TestClass]
    public class ChampionControllerTest
    {

        ChampionsController api = new ChampionsController(new StubData());

        [TestMethod]
        public async Task Get_Default_OkList()
        {

            List<ChampionDTO> list = new List<ChampionDTO> {new ChampionDTO("Akali","","","Assassin",""), new ChampionDTO("Aatrox", "", "", "Fighter",""), new ChampionDTO("Ahri", "", "", "Mage",""), new ChampionDTO("Akshan", "", "", "Marksman",""), new ChampionDTO("Bard", "", "","Support",""), new ChampionDTO("Alistar", "", "","Tank","") };
            IActionResult a = await api.Get();
            a.Should().NotBeNull();
            var aObject = a as OkObjectResult;
            aObject.Should().NotBeNull();
            var championresult = aObject.Value as IEnumerable<ChampionDTO>;
            list.Should().BeEquivalentTo(championresult);
        }

        [TestMethod]
        public async Task Get_MoreThanLimit_BadRequest()
        {
            IActionResult a = await api.Get(index :0,size :11);
            
            a.Should().NotBeNull();
            a.Should().BeOfType<BadRequestResult>();
        }

        [TestMethod]
        public async Task Get_2First_OkListOf2()
        {
            List<ChampionDTO> list = new List<ChampionDTO> { new ChampionDTO("Akali", "", "", "Assassin",""), new ChampionDTO("Aatrox", "", "", "Fighter","") };

            IActionResult a = await api.Get(index: 0,size: 2);

            a.Should().NotBeNull();
            a.Should().BeOfType<OkObjectResult>();
            var aObject = a as OkObjectResult;
            aObject.Should().NotBeNull();
            var championresult = aObject.Value as IEnumerable<ChampionDTO>;
            list.Should().BeEquivalentTo(championresult);
        }

        [TestMethod]
        public async Task Get_FilterAName_OkListOf5()
        {
            List<ChampionDTO> list = new List<ChampionDTO> { new ChampionDTO("Akali", "", "", "Assassin", ""), new ChampionDTO("Akshan", "", "", "Marksman", "") };

            IActionResult a = await api.Get(name: "Ak");

            a.Should().NotBeNull();
            a.Should().BeOfType<OkObjectResult>();
            var aObject = a as OkObjectResult;
            aObject.Should().NotBeNull();
            var championresult = aObject.Value as IEnumerable<ChampionDTO>;
            list.Should().BeEquivalentTo(championresult);
        }



        [TestMethod]
        public async Task Post_ValidChampion_Created()
        {
            ChampionsController api = new ChampionsController(new StubData());
            IActionResult a = await api.Post(new ChampionDTO("nom","bio","icon", "Assassin",""));
            var action = (CreatedAtActionResult)a;
            var champAction = action.Value as IEnumerable<ChampionDTO>;
            Assert.IsNotNull(a);
            ChampionDTO champ = new ChampionDTO("nom", "bio", "icon","Assassin", "");
            Assert.IsTrue(champ.equals(other: (ChampionDTO)((CreatedAtActionResult)a).Value));
        }

    }
}