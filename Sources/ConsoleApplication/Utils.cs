﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    public static class Utils
    {
        public static void showMainMenu()
        {
            Console.WriteLine("-------- Menu -------");
            Console.WriteLine(" 1 - Champions ");
            Console.WriteLine(" 2 - Skills   ");
            Console.WriteLine("\n 9 - Quitter");
        }

        public static void showChampionMenu()
        {
            Console.WriteLine("-------- Champion -------");
            Console.WriteLine(" 1 - Count ");
            Console.WriteLine(" 2 - Default   ");
            Console.WriteLine("\n 9 - Quitter");
        }

        public static async Task championMenu(IChampionsManager championsManager ) {
            string choix = "0";

            while (choix != "9")
            {
                Utils.showChampionMenu();
                choix = Console.ReadLine();

                switch (choix)
                {
                    case "1":
                        var json = await championsManager.GetNbItems();
                        Console.WriteLine("# result : "+ json);
                        break;
                    case "2":
                        var list = await championsManager.GetItems(0, 10);
                        foreach(var cham in  list)
                        {
                            Console.WriteLine("# result : " +cham.ToString());

                        }
                        break;
                }
            }
        }


    }
}
