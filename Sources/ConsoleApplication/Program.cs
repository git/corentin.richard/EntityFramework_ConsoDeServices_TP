﻿
using ConsoleApplication;
using EntityFramework;
using HttpClient;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Model;

var builder = WebApplication.CreateBuilder();

builder.Services.AddDbContext<LoLDBContextWithStub>();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetService<LoLDBContextWithStub>();
    context.Database.EnsureCreated();
}

IDataManager dataManager = new HttpClientManager();

string choice = "0";

while (choice != "9")
{
    Utils.showMainMenu();
    choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            {
                Utils.championMenu(dataManager.ChampionsMgr);
                break;
            }
        case "2": 
            {
                //Utils.
                break;
            }

    }
}