﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
   
    public class SkillEntity
    {
        public SkillType Type { get; set; }
        
        [Key]
        public string Name { get; set; }
        //public string Name
        //{
        //    get => name;
        //    private init
        //    {
        //        if (string.IsNullOrWhiteSpace(value))
        //        {
        //            throw new ArgumentException("a Skill needs a name");
        //        }
        //        name = value;
        //    }
        //}
        //private readonly string name = null!;

        public string Description { get; set; }
        //public string Description
        //{
        //    get => description;
        //    set
        //    {
        //        if (string.IsNullOrWhiteSpace(value))
        //        {
        //            description = "";
        //            return;
        //        }
        //        description = value;
        //    }
        //}
        //private string description = "";

        //public SkillEntity(string Name, string Description, SkillType Type) { 
        //    this.name = Name;
        //    this.Description = Description;
        //    this.Type = Type;
        //}


        // One to many with champion :
        public ChampionEntity Champion { get; set; }
    }
}
