﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    public class SkinEntity //ONE TO MANY
    {
        
        [Key]
        public string? Name { get; set; }

        public string? Description { get; set; }

        //public string Name
        //{
        //    get => name;
        //    private init
        //    {
        //        if (string.IsNullOrWhiteSpace(value))
        //        {
        //            throw new ArgumentException("A skin must have a name");
        //        }
        //        name = value;
        //    }
        //}
        //private readonly string name = null!;

        //public string Description
        //{
        //    get => description;
        //    set
        //    {
        //        if (string.IsNullOrWhiteSpace(value))
        //        {
        //            description = "";
        //            return;
        //        }
        //        description = value;
        //    }
        //}
        //private string description = "";

        public string Icon { get; set; } = "";


        //public LargeImageEntity Image { get; set; }
        public string? Image { get; set; }


        public float Price { get; set; }
        public ChampionEntity Champion { get; set; }


        //public ChampionEntity Champion
        //{
        //    get => champion;
        //    private init
        //    {
        //        if (value == null)
        //            throw new ArgumentNullException("A skill can't have a null champion");
        //        champion = value;
        //    }
        //}
        //private readonly ChampionEntity champion = null!;

        //public SkinEntity(string name, ChampionEntity champion, float price = 0.0f, string icon = "", string image = "", string description = "")
        //{
        //    Name = name;
        //    Champion = champion;
        //    //Champion.AddSkin(this);
        //    Price = price;
        //    Icon = icon;
        //    Image = new LargeImageEntity(image);
        //    Description = description;
        //}
    }
}
