﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    
    public class LargeImageEntity
    {
        public int Id { get; set; }
        public string Base64 { get; set; }

        //public LargeImageEntity(string base64)
        //{
        //    Base64 = base64;
        //}
    }
}
