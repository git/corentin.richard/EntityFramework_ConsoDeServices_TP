﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    public enum EnumCategory
    {
        Major,
        Minor1,
        Minor2,
        Minor3,
        OtherMinor1,
        OtherMinor2
    }
}
