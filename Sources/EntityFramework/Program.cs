﻿// See https://aka.ms/new-console-template for more information
using EntityFramework;
using EntityFramework.Manager;
using Microsoft.EntityFrameworkCore;
using Model;
using System.Buffers.Text;

using ( var context = new LoLDbContext())
{
    //context.Add(new ChampionEntity{ Name = "test", Bio = "test", Icon = "test" } );
    context.SaveChanges();

    ChampionEntity champ = context.Find<ChampionEntity>("Akali");

    if( champ != null)
    {
    Console
        .WriteLine(champ.ToString());

    }
    else
    {
        Console.WriteLine("Not Found");
    }

    //Test BDD Skills
    ChampionEntity champSkill = new ChampionEntity { Name="nomSkill", Bio="bioSkill", Icon="iconSkill" };

    //SkillEntity s1 = new SkillEntity { Name = "Skill1", Description = "desc", Type = SkillType.Unknown };
    SkillEntity s2 = new SkillEntity { Name="Skill2", Description="desc2", Type= EntityFramework.SkillType.Ultimate };
    SkillEntity s3 = new SkillEntity { Name = "Skill3", Description = "desc3", Type = EntityFramework.SkillType.Passive };

    champSkill.AddSkill(new SkillEntity { Name = "Skill1", Description = "desc", Type = EntityFramework.SkillType.Unknown });
    champSkill.AddSkill(s2);
    champSkill.AddSkill(s3);

    context.Add(champSkill);

    context.SaveChanges();

    IDataManager dataManager = new EFDataManager();
    IChampionsManager championsManager = dataManager.ChampionsMgr;
    IEnumerable<Champion?> champions = await championsManager.GetItemsByName("A", 0, 1);
    //Console.WriteLine(champions.First().Name);


    //using ( var context = new LoLDbContext())
    //{
    //    //context.Add(new ChampionEntity{ Name = "test", Bio = "test", Icon = "test" } );
    //    context.SaveChanges();

    //    ChampionEntity champ = context.Find<ChampionEntity>("Akali");

    //    if( champ != null)
    //    {
    //    Console
    //        .WriteLine(champ.ToString());

    //    }
    //    else
    //    {
    //        Console.WriteLine("Not Found");
    //    }

    //    //Test BDD Skills
    //    ChampionEntity champSkill = new ChampionEntity { Name="nomSkill", Bio="bioSkill", Icon="iconSkill" };

    //    //SkillEntity s1 = new SkillEntity { Name = "Skill1", Description = "desc", Type = SkillType.Unknown };
    //    SkillEntity s2 = new SkillEntity { Name="Skill2", Description="desc2", Type=SkillType.Ultimate };
    //    SkillEntity s3 = new SkillEntity { Name = "Skill3", Description = "desc3", Type = SkillType.Passive };

    //    champSkill.AddSkill(new SkillEntity { Name = "Skill1", Description = "desc", Type = SkillType.Unknown });
    //    champSkill.AddSkill(s2);
    //    champSkill.AddSkill(s3);

    //    context.Add(champSkill);

    //    context.SaveChanges();


    //    //OneToMany
    //    Console.WriteLine("Champions : ");
    //    foreach (var champi in context.Champions.Include(a => a.skins))
    //    {
    //        Console.WriteLine($"\t{champi.Name} : {champi.Bio}");
    //        foreach (var s in champi.skins)
    //        {
    //            Console.WriteLine($"\t\t{s.Name}");
    //        }
    //    }

    //    Console.WriteLine();

    //    Console.WriteLine("Skin :");
    //    foreach (var s in context.Skins)
    //    {
    //        Console.WriteLine($"\t{s.Name}: {s.Description} (Champion : {s.Champion.Name})");
    //    }

    Console.WriteLine("\nAjout d'un Champion et 6 Skins...\n");

    ChampionEntity captainMarvel = new ChampionEntity { Name = "Captain Marvel", Bio = "Mais que fait un avenger ici ??", Icon = "Icon.png" };
    SkinEntity[] skins = {  new SkinEntity {Name = "La Fiesta", Champion = captainMarvel},
                            new SkinEntity { Name = "Five Hundred Miles High", Champion = captainMarvel },
                            new SkinEntity { Name = "Captain Marvel", Champion = captainMarvel },
                            new SkinEntity { Name = "Time's Lie", Champion = captainMarvel },
                            new SkinEntity { Name = "Lush Life", Champion = captainMarvel },
                            new SkinEntity { Name = "Day Waves", Champion = captainMarvel }
                        };
    foreach (var s in skins)
    {
        captainMarvel.skins.Add(s);
    }

    context.Add(captainMarvel);
    context.SaveChanges();


    //OnetoMany Skill
    ChampionEntity Levram = new ChampionEntity { Name = "Captain Levram", Bio="bio", Icon="/img" };
    SkillEntity[] morceaux = {  new SkillEntity { Name = "La Fiesta", Description="SkillDesc", Type=EntityFramework.SkillType.Unknown, Champion= Levram },
                                new SkillEntity { Name = "berserk", Description="SkillDesc1", Type=EntityFramework.SkillType.Unknown, Champion= Levram },
                                new SkillEntity { Name = "taunt", Description = "SkillDesc2", Type = EntityFramework.SkillType.Unknown, Champion = Levram },
                                new SkillEntity { Name = "fear", Description = "SkillDesc3", Type = EntityFramework.SkillType.Unknown, Champion = Levram },
                                new SkillEntity { Name = "flashHeal", Description = "SkillDesc4", Type = EntityFramework.SkillType.Unknown, Champion = Levram },
                                new SkillEntity { Name = "bubbuletp", Description = "SkillDesc5", Type = EntityFramework.SkillType.Unknown, Champion = Levram }
    };
    foreach (var m in morceaux)
    {
        Levram.Skills.Add(m);
    }
        
    context.Add(Levram);
    context.SaveChanges();



    var r1 = new RuneEntity { Name = "Rune1", Description = "aaa", Family = EnumRuneFamily.Domination, Image = new LargeImage("base") };
    var r2 = new RuneEntity { Name = "Rune2", Description = "aaa", Family = EnumRuneFamily.Domination, Image = new LargeImage("base") };
    var corichard = new ChampionEntity { Name = "Corichard", Bio = "biobio", Icon = "Icon.png" };
    var pintrand = new ChampionEntity { Name = "Pintrand", Bio = "biobio", Icon = "Icon.png" };
    var rp1 = new RunePageEntity { Name = "RP1", Rune = new RuneEntity { Name = "aa", Description = "aaa", Family = EnumRuneFamily.Domination, Image = new LargeImage("base") }, Champion = new List<ChampionEntity> { corichard } };
    var rp2 = new RunePageEntity { Name = "RP2", Rune = new RuneEntity{ Name = "aaa", Description = "aaa", Family = EnumRuneFamily.Domination, Image = new LargeImage("base") }, Champion = new List<ChampionEntity> { pintrand } };

    context.Rune.AddRange(new[] { r1, r2 });
    context.Champions.AddRange(new[] { corichard, pintrand });
    context.RunePage.AddRange(new[] { rp1, rp2 });
    context.SaveChanges();
}