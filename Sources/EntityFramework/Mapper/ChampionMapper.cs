﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Mapper
{
    public static class ChampionMapper
    {
        public static ChampionEntity ToEntity(this Champion champion) {
            return new ChampionEntity { Name = champion.Name, Bio = champion.Bio, Icon = champion.Icon };
        }

        public static Champion ToChampion(this ChampionEntity champion)
        {
            return new Champion(champion.Name,bio: champion.Bio,icon: champion.Icon);
        }

    }
}
