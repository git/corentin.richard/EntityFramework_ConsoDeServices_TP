﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Mapper
{
    public static class SkinMapper
    {
        public static SkinEntity ToEntity(this Skin skin)
        {
            return new SkinEntity { Champion = skin.Champion.ToEntity(), Description = skin.Description, Icon = skin.Icon, Image = skin.Image.Base64, Name = skin.Name, Price = skin.Price };
        }

        public static Skin ToSkin(this SkinEntity entity)
        {
            return new Skin(entity.Name,entity.Champion.ToChampion(),price: entity.Price,icon: entity.Icon, image: entity.Image,description: entity.Description);
        }


    }
}
