﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Model.RunePage;

namespace EntityFramework
{
    public class RunePageEntity
    {
        [Key]
        public string Name { get; set; }

        public RuneEntity? Rune { get; set; }

        //? voir si cela pause probleme
        //public Dictionary<EnumCategory, Rune> Dico = new Dictionary<EnumCategory, Rune>();

        // One to many pour l'instant, voir si on retransforme en dico :
        public ICollection<RuneEntity> Runes { get; set; }


        // Pour le many to many Champion *<---->* RunePage
        public ICollection<ChampionEntity> Champion{ get; set; }



        public void CheckRunes(EnumCategory newRuneCategory){}
        public void CheckFamilies(EnumCategory cat1, EnumCategory cat2){}
        public void UpdateMajorFamily(EnumCategory minor, bool expectedValue){}
    }
}
