﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EntityFramework.Migrations
{
    /// <inheritdoc />
    public partial class MigrSkill : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Champions",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
                    Bio = table.Column<string>(type: "string", maxLength: 500, nullable: false),
                    Icon = table.Column<string>(type: "TEXT", nullable: false),
                    Image = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Champions", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Base64 = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Type = table.Column<int>(type: "INTEGER", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false),
                    ChampionEntityToSkillForeignKey = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Skills_Champions_ChampionEntityToSkillForeignKey",
                        column: x => x.ChampionEntityToSkillForeignKey,
                        principalTable: "Champions",
                        principalColumn: "Name");
                });

            migrationBuilder.CreateTable(
                name: "Skins",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: true),
                    Icon = table.Column<string>(type: "TEXT", nullable: false),
                    Image = table.Column<string>(type: "TEXT", nullable: true),
                    Price = table.Column<float>(type: "REAL", nullable: false),
                    ChampionEntityForeignKey = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skins", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Skins_Champions_ChampionEntityForeignKey",
                        column: x => x.ChampionEntityForeignKey,
                        principalTable: "Champions",
                        principalColumn: "Name");
                });

            migrationBuilder.CreateTable(
                name: "ChampionEntityRunePageEntity",
                columns: table => new
                {
                    ChampionName = table.Column<string>(type: "TEXT", nullable: false),
                    RunePageEntitiesName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChampionEntityRunePageEntity", x => new { x.ChampionName, x.RunePageEntitiesName });
                    table.ForeignKey(
                        name: "FK_ChampionEntityRunePageEntity_Champions_ChampionName",
                        column: x => x.ChampionName,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rune",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    RuneForeignKey = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rune", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "RunePage",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    RuneName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RunePage", x => x.Name);
                    table.ForeignKey(
                        name: "FK_RunePage_Rune_RuneName",
                        column: x => x.RuneName,
                        principalTable: "Rune",
                        principalColumn: "Name");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChampionEntityRunePageEntity_RunePageEntitiesName",
                table: "ChampionEntityRunePageEntity",
                column: "RunePageEntitiesName");

            migrationBuilder.CreateIndex(
                name: "IX_Rune_RuneForeignKey",
                table: "Rune",
                column: "RuneForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_RunePage_RuneName",
                table: "RunePage",
                column: "RuneName");

            migrationBuilder.CreateIndex(
                name: "IX_Skills_ChampionEntityToSkillForeignKey",
                table: "Skills",
                column: "ChampionEntityToSkillForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_Skins_ChampionEntityForeignKey",
                table: "Skins",
                column: "ChampionEntityForeignKey");

            migrationBuilder.AddForeignKey(
                name: "FK_ChampionEntityRunePageEntity_RunePage_RunePageEntitiesName",
                table: "ChampionEntityRunePageEntity",
                column: "RunePageEntitiesName",
                principalTable: "RunePage",
                principalColumn: "Name",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rune_RunePage_RuneForeignKey",
                table: "Rune",
                column: "RuneForeignKey",
                principalTable: "RunePage",
                principalColumn: "Name");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rune_RunePage_RuneForeignKey",
                table: "Rune");

            migrationBuilder.DropTable(
                name: "ChampionEntityRunePageEntity");

            migrationBuilder.DropTable(
                name: "Image");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "Skins");

            migrationBuilder.DropTable(
                name: "Champions");

            migrationBuilder.DropTable(
                name: "RunePage");

            migrationBuilder.DropTable(
                name: "Rune");
        }
    }
}
