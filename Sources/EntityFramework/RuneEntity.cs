﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    //[Table("Rune")]
    public class RuneEntity
    {
        [Key]
        public string Name;

        public string Description;

        public EnumRuneFamily Family;

        public LargeImage Image;


        //OtM
        public RunePageEntity RunePage { get; set; }
    }
}
