﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    public class StubbedContext : LoLDbContext
    {
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.EnableSensitiveDataLogging();
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ChampionEntity corichard = new ChampionEntity { Name = "Corichard", Bio = "biobiobiobio", Icon = "/a/a/a/a" };
            ChampionEntity pintrand = new ChampionEntity { Name = "Pintrand", Bio = "mimimimimim", Icon = "/small.png" };

            RuneEntity r1 = new RuneEntity { Name = "FirstRune", Description = "desc", Family = EnumRuneFamily.Domination };
            RuneEntity r2 = new RuneEntity { Name = "SecondRune", Description = "desc", Family = EnumRuneFamily.Unknown };

            //ChampionEntity corichard = new ChampionEntity() { Name = "Corichard", Bio = "biobiobiobio", Icon = "/a/a/a/a", Image = new LargeImageEntity { Base64 = "base" } };
            //ChampionEntity pintrand = new ChampionEntity { Name = "Pintrand", Bio = "mimimimimim", Icon = "/small.png", Image = new LargeImageEntity { Base64 = "base" } };

            modelBuilder.Entity<ChampionEntity>().HasData(corichard, pintrand);

            modelBuilder.Entity<SkinEntity>().HasData(new { Name = "aaaa", ChampionEntityForeignKey = "Corichard", Description = "So What", Icon="/Icon.png", Price=10.0f },
                                            new { Name = "skin", ChampionEntityForeignKey = "Corichard", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "bo", ChampionEntityForeignKey = "Corichard", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "Joulie", ChampionEntityForeignKey = "Corichard", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "Radiance", ChampionEntityForeignKey = "Corichard", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "void", ChampionEntityForeignKey = "Corichard", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "Radiance", ChampionEntityForeignKey = "Pintrand", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "void", ChampionEntityForeignKey = "Pintrand", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "DarkTheme", ChampionEntityForeignKey = "Pintrand", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "gold", ChampionEntityForeignKey = "Pintrand", Description = "So What", Icon = "/Icon.png", Price = 10.0f },
                                            new { Name = "ruby", ChampionEntityForeignKey = "Pintrand", Description = "So What", Icon = "/Icon.png", Price = 10.0f }
            );

            //Skills
            modelBuilder.Entity<SkillEntity>().HasData(new { Name="Skill", Description="Desc", Type= SkillType.Basic, ChampionEntityToSkillForeignKey= "Corichard" },
                                     new { Name = "Skill2", Description = "Desc", Type = SkillType.Basic, ChampionEntityToSkillForeignKey = "Corichard" },
                                     new { Name = "Skill3", Description = "Desc", Type = SkillType.Passive, ChampionEntityToSkillForeignKey = "Corichard" },
                                     new { Name = "Skill4", Description = "Desc", Type = SkillType.Unknown, ChampionEntityToSkillForeignKey = "Corichard" },
                                     new { Name = "Skill5", Description = "Desc", Type = SkillType.Basic, ChampionEntityToSkillForeignKey = "Corichard" },
                                     new { Name = "Skill6", Description = "Desc", Type = SkillType.Basic, ChampionEntityToSkillForeignKey = "Corichard" },
                                     new { Name = "Skill10", Description = "Desc", Type = SkillType.Basic, ChampionEntityToSkillForeignKey = "Pintrand" },
                                     new { Name = "Skill11", Description = "Desc", Type = SkillType.Unknown, ChampionEntityToSkillForeignKey = "Pintrand" },
                                     new { Name = "Skill12", Description = "Desc", Type = SkillType.Passive, ChampionEntityToSkillForeignKey = "Pintrand" },
                                     new { Name = "Skill13", Description = "Desc", Type = SkillType.Basic, ChampionEntityToSkillForeignKey = "Pintrand" },
                                     new { Name = "Skill14", Description = "Desc", Type = SkillType.Basic, ChampionEntityToSkillForeignKey = "Pintrand" }
            );




            modelBuilder.Entity<RunePageEntity>().HasData(new { Name = "RP1", Rune = r1, Champion = corichard},
                                                new { Name = "RP2", Rune = r2, Champion = pintrand}
            );
        
            
            
            RunePageEntity rp11 = new RunePageEntity { Name = "rp11"};
            RunePageEntity rp21 = new RunePageEntity { Name = "rp21"};

            modelBuilder.Entity<RunePageEntity>().HasData(rp11, rp21);

            modelBuilder.Entity<RuneEntity>().HasData(new { Name = "Rune", Description = "Desc", Famille = EnumRuneFamily.Domination, RuneForeignKey = "rp11" },
                                    new{Name = "Rune2", Description = "Desc",Famille = EnumRuneFamily.Domination,RuneForeignKey = "rp11"},
                                    new { Name = "Rune3", Description = "Desc", Famille = EnumRuneFamily.Domination, RuneForeignKey = "rp21" },
                                    new { Name = "Rune4", Description = "Desc", Famille = EnumRuneFamily.Domination, RuneForeignKey = "rp21" }
                );
        }




    }
}
