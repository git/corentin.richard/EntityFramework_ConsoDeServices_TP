﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Manager
{
    public partial class EFDataManager : IDataManager
    {
        public EFDataManager() {
            ChampionsMgr = new ChampionsManager(this);
            SkinsMgr = new SkinsManager(this);
        }
        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager SkinsMgr { get; }

        public IRunesManager RunesMgr { get; }

        public IRunePagesManager RunePagesMgr { get; }

    }
}
