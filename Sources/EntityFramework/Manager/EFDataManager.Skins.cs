﻿using EntityFramework.Mapper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Manager
{
    public partial class EFDataManager
    {
        public class SkinsManager : ISkinsManager
        {
            private readonly EFDataManager parent;

            public SkinsManager(EFDataManager parent)
            {
                this.parent = parent;
            }

            public Task<Skin?> AddItem(Skin? item)
            {
                throw new NotImplementedException();
            }

            public Task<bool> DeleteItem(Skin? item)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Skin?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public async Task<IEnumerable<Skin?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                using (var context = new LoLDBContextWithStub())
                {
                    var skins = context.Skins.Where(c => c.Champion.Equals(champion)).ToList();

                    if (descending == false)
                    {
                        return skins.Select(c => c.ToSkin()).ToList().Skip(index * count).Take(count).OrderBy(c => c.Name);

                    }
                    else
                    {
                        return skins.Select(c => c.ToSkin()).ToList().Skip(index * count).Take(count).OrderByDescending(c => c.Name);

                    }
                }
            }

            public Task<IEnumerable<Skin?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItems()
            {
                throw new NotImplementedException();
            }

            public async Task<int> GetNbItemsByChampion(Champion? champion)
            {
                using(var context = new LoLDBContextWithStub())
                {
                    return context.Skins.Where(c => c.Champion.Equals(champion.ToEntity())).Count();
                }
            }

            public Task<int> GetNbItemsByName(string substring)
            {
                throw new NotImplementedException();
            }

            public Task<Skin?> UpdateItem(Skin? oldItem, Skin? newItem)
            {
                throw new NotImplementedException();
            }
        }
    }
}
