﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    public enum ChampionClass
    {
        Unknown,
        Assassin,
        Fighter,
        Mage,
        Marksman,
        Support,
        Tank
    }
}
