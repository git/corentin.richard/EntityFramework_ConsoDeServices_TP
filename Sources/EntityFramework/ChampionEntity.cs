﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    [Table("Champion")]
    public class ChampionEntity
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int Id { get; set; }

        [Key]
        [MaxLength(50)]
        public string Name { get; set; }

        
        [MaxLength(500)]
        [Column("Bio", TypeName = "string")]
        public string Bio { get; set; }

        [Required]
        public string Icon { get; set; }

        //public ImmutableHashSet<Skill> Skills => skills.ToImmutableHashSet();
        //private HashSet<Skill> skills = new HashSet<Skill>();

        public ICollection<SkillEntity> Skills { get; set; } = new Collection<SkillEntity>();

        //public ReadOnlyCollection<SkinEntity> Skins { get; private set; }
        //private List<SkinEntity> skins = new();

        public ICollection<SkinEntity> skins { get; set; } = new Collection<SkinEntity>();

        //public LargeImageEntity? Image { get; set; }  ====> voir pour faire "plus propre" => créé une table pour l'entity Largeimage
        public string? Image { get; set; }

        // Pour le many to many Champion *<---->* RunePage
        public ICollection<RunePageEntity> RunePageEntities{ get; set; }



        /// <summary>
        /// pas besoin de constructeur ! (sans lui, il est possible d'utiliser la syntaxe utilisé dans le stubbedbDBCOntext)
        /// </summary>
        /// <returns></returns>
        //public ChampionEntity(string name,string bio,string icon) {
        //    this.Name = name;
        //    this.Bio = bio;
        //    this.Icon = icon;
        //    Skills= new List<SkillEntity>();
        //    //Skins = new ReadOnlyCollection<SkinEntity>(skins);
        //}

        public override string ToString()
        {
            return Name;
        }



        public void AddSkill(SkillEntity skill)
            => Skills.Add(skill);

        public void RemoveSkill(SkillEntity skill)
            => Skills.Remove(skill);

        public bool AddSkin(SkinEntity skin)
        {
            if (skins.Contains(skin))
                return false;
            skins.Add(skin);
            return true;
        }


    }
}
