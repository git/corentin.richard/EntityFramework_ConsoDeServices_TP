﻿using EntityFramework.Mapper;
using Microsoft.EntityFrameworkCore;
using StubLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    public class LoLDBContextWithStub : LoLDbContext
    {
        protected override async void OnModelCreating(ModelBuilder modelBuilder) 
        {
            base.OnModelCreating(modelBuilder);
            var stub = new StubData.ChampionsManager(new StubData());
            var list = await stub.GetItems(0, await stub.GetNbItems());
            modelBuilder.Entity<ChampionEntity>().HasData(
                list.Select(champion => champion.ToEntity())
                );
        }
    }
}
