﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EntityFramework
{
    public class LoLDbContext : DbContext
    {
        public DbSet<ChampionEntity> Champions { get; set; }

        public DbSet<SkinEntity> Skins { get; set; }

        public DbSet<SkillEntity> Skills { get; set; }

        public DbSet<LargeImageEntity> Image { get; set; }

        public DbSet<RuneEntity> Rune { get; set; }

        public DbSet<RunePageEntity> RunePage { get; set; }

        public LoLDbContext()
        { }

        public LoLDbContext(DbContextOptions<LoLDbContext> options)
            : base(options)
        { }


        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlite("Data Source=champion.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChampionEntity>().HasKey(entity => entity.Name);
            modelBuilder.Entity<ChampionEntity>().ToTable("Champions");

            //modelBuilder.Entity<ChampionEntity>().Property(entity => entity.Id)
            //    .ValueGeneratedOnAdd();

            modelBuilder.Entity<ChampionEntity>().Property(entity => entity.Name)
                .IsRequired()
                .HasMaxLength(50)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<ChampionEntity>().Property(entity => entity.Bio)
                .HasMaxLength(500)
                .HasColumnName("Bio")
                .HasColumnType("string");

            modelBuilder.Entity<ChampionEntity>().Property(entity => entity.Icon)
                .IsRequired();



            /// One to many
            /// ChampionEntity 1 ---> * SkinEntity
            //création de la table Skin
            modelBuilder.Entity<SkinEntity>().HasKey(skin => skin.Name); //définition de la clé primaire
            modelBuilder.Entity<SkinEntity>().Property(skin => skin.Name)
                                            .ValueGeneratedOnAdd(); //définition du mode de génération de la clé : génération à l'insertion

            // Add the shadow property to the model
            modelBuilder.Entity<SkinEntity>()
                .Property<string>("ChampionEntityForeignKey");

            // Use the shadow property as a foreign key
            modelBuilder.Entity<SkinEntity>()
                .HasOne(skin => skin.Champion)  
                .WithMany(champion => champion.skins)
                .HasForeignKey("ChampionEntityForeignKey");


            /// One to many
            /// ChampionEntity ---> * SkillEntity
            //création de la table ChampionEntity
            //modelBuilder.Entity<ChampionEntity>().HasKey(c => c.Name); //définition de la clé primaire
            //modelBuilder.Entity<ChampionEntity>().Property(c => c.Name)
            //                               .ValueGeneratedOnAdd(); //définition du mode de génération de la clé : génération à l'insertion

            //création de la table SkillEntity
            modelBuilder.Entity<SkillEntity>().HasKey(s => s.Name); //définition de la clé primaire
            modelBuilder.Entity<SkillEntity>().Property(s => s.Name)
                                            .ValueGeneratedOnAdd(); //définition du mode de génération de la clé : génération à l'insertion

            // Add the shadow property to the model
            modelBuilder.Entity<SkillEntity>()
                .Property<string>("ChampionEntityToSkillForeignKey");

            // Use the shadow property as a foreign key
            modelBuilder.Entity<SkillEntity>()
                .HasOne(s => s.Champion)
                .WithMany(c => c.Skills)
                .HasForeignKey("ChampionEntityToSkillForeignKey");



            /// Many to Many ChampionEntity - RunePageEntity
            modelBuilder.Entity<RunePageEntity>().HasKey(entity => entity.Name);
            modelBuilder.Entity<RunePageEntity>().HasKey(entity => entity.Name);
            modelBuilder.Entity<RunePageEntity>().ToTable("RunePage");


            // Use the shadow property as a foreign key
            modelBuilder.Entity<RunePageEntity>()
                .HasMany(r => r.Champion)
                .WithMany(c => c.RunePageEntities);
            //.HasForeignKey("AlbumForeignKey");

            modelBuilder.Entity<ChampionEntity>()
                .HasMany(c => c.RunePageEntities)
                .WithMany(r => r.Champion);
            //.HasForeignKey("AlbumForeignKey");



            //création de la table RuneEntity
            modelBuilder.Entity<RuneEntity>().HasKey(r => r.Name); //définition de la clé primaire
            modelBuilder.Entity<RuneEntity>().Property(r => r.Name)
                                            .ValueGeneratedOnAdd(); //définition du mode de génération de la clé : génération à l'insertion

            modelBuilder.Entity<RuneEntity>()
                .Property<string>("RuneForeignKey");

            modelBuilder.Entity<RuneEntity>()
                .HasOne(r => r.RunePage)
                .WithMany(rp => rp.Runes)
                .HasForeignKey("RuneForeignKey");


        }
    }
}
